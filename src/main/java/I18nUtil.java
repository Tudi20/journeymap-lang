import java.io.BufferedWriter;
import java.io.File;
import java.io.StringReader;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.TreeMap;

/**
 * Simple utility to keep resource bundle files organized.
 * Alphabetizes .lang files, removes unused keys, adds lines for missing keys.
 */
public class I18nUtil
{
    /**
     * Runs the utility on all .lang files.
     *
     * @param args ignored
     * @throws Exception
     */
    public static void main(String[] args) throws Exception
    {
        File langDir = new File("src/main/resources/assets/journeymap/lang").getCanonicalFile();
        File english = new File(langDir, "en_US.lang");

        // Alphabetize english file first
        Map<String, String> englishMap = getMap(english);
        updateFile(english, getHeader(english), englishMap);
        System.out.println("Alphabetized " + english);

        // Sync other languages to english
        File dir = english.getParentFile();
        File[] files = dir.listFiles();
        for (File other : files)
        {
            if (!other.equals(english) && other.getName().endsWith(".lang"))
            {
                syncToEnglish(englishMap, other);
            }
        }
    }

    /**
     * Get a TreeMap of properties from .lang file
     */
    private static TreeMap<String, String> getMap(File propertiesFile) throws Exception
    {
        // Preserve slashes in original
        List<String> lines = Files.readAllLines(propertiesFile.toPath(), Charset.forName("utf-8"));
        StringBuilder sb = new StringBuilder();
        for (String line : lines)
        {
            sb.append(line.replace("\\", "\\\\")).append('\n');
        }

        // Load into properties as name/value pairs
        Properties props = new Properties();
        props.load(new StringReader(sb.toString()));

        // But use as a TreeMap to keep keys alphabetized
        TreeMap<String, String> map = new TreeMap<>();
        for (Map.Entry<Object, Object> prop : props.entrySet())
        {
            if (prop.toString().contains("\\"))
            {
                map.put((String) prop.getKey(), (String) prop.getValue());
            }
            map.put((String) prop.getKey(), (String) prop.getValue());
        }
        return map;
    }

    /**
     * Get the first lines in the file with # in them.
     */
    public static String getHeader(File propertiesFile) throws Exception
    {
        StringBuilder sb = new StringBuilder();
        List<String> lines = Files.readAllLines(propertiesFile.toPath(), Charset.forName("utf-8"));
        for (String line : lines)
        {
            if (line == null)
            {
                continue;
            }
            String trimmed = line.trim();
            if (trimmed.length() > 0 && !trimmed.contains("#"))
            {
                break;
            }
            sb.append(line).append('\n');
        }
        return sb.toString();
    }

    /**
     * Overwrite the file with the header and properties provided.
     */
    public static void updateFile(File propertiesFile, String header, Map<String, String> properties) throws Exception
    {
        StringBuilder sb = new StringBuilder(header);

        for (Map.Entry<String, String> prop : properties.entrySet())
        {
            String key = prop.getKey();
            String value = prop.getValue();
            if (value.startsWith("#"))
            {
                sb.append("# ");
                value = value.replace("#", "");
            }
            sb.append(key).append("=").append(value).append('\n');
        }

        BufferedWriter writer = Files.newBufferedWriter(propertiesFile.toPath(), Charset.forName("utf-8"));
        writer.append(sb);
        writer.flush();
        writer.close();
    }

    /**
     * Synchronize properties of other language to English.
     */
    public static void syncToEnglish(Map<String, String> englishMap, File otherFile) throws Exception
    {
        Map<String, String> otherMap = getMap(otherFile);

        // Remove keys not in the English file
        otherMap.keySet().retainAll(englishMap.keySet());

        // Put in placeholder
        for (String key : englishMap.keySet())
        {
            if (!otherMap.containsKey(key))
            {
                otherMap.put(key, "#" + englishMap.get(key));
            }
        }

        updateFile(otherFile, getHeader(otherFile), otherMap);
        System.out.println("Synchronized " + otherFile);
    }

}
